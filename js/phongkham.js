var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng khám Đa khoa Nguyễn An Phúc",
   "address": "613 Đồng Khởi, Tân Phong, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9850833,
   "Latitude": 106.8464297
 },
 {
   "STT": 2,
   "Name": "Phòng khám Đa khoa Dân Y",
   "address": "Quốc lộ 51, Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9012368,
   "Latitude": 106.8526612
 },
 {
   "STT": 3,
   "Name": "Phòng khám Đa khoa Tân Long",
   "address": "06-23-24/F6 Quốc lộ 51, Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9007904,
   "Latitude": 106.8537814
 },
 {
   "STT": 4,
   "Name": "Phòng khám Đa khoa Y Đức",
   "address": "1/2 khu phố 8, Tân Phong, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9683726,
   "Latitude": 106.9123465
 },
 {
   "STT": 5,
   "Name": "Phòng khám Đa khoa Tam Đức",
   "address": "B12B1/4A – B12/4A khu phố 4, Tân Hiệp, Biên Hòa, Đồng Nai ",
   "Longtitude": 10.9727257,
   "Latitude": 106.9085495
 },
 {
   "STT": 6,
   "Name": "Phòng khám Đa khoa Ái Nghĩa - Chi nhánh 1",
   "address": "C4, C5 Đồng Khởi, Tân Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9569613,
   "Latitude": 106.8606563
 },
 {
   "STT": 7,
   "Name": "Phòng khám Đa khoa Sinh Hậu",
   "address": "27/13 Bùi Trọng Nghĩa, Trảng Dài, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9835441,
   "Latitude": 106.8614007
 },
 {
   "STT": 8,
   "Name": "Phòng khám Đa khoa - Bác sĩ Bành Kim Linh",
   "address": "385 xa lộ ĐỒNG NAI, Hố Nai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9641156,
   "Latitude": 106.8837843
 },
 {
   "STT": 9,
   "Name": "Phòng khám Đa khoa - Trường Cao đẳng Y tế Đồng Nai",
   "address": "5/4 Đồng Khởi, Tân Mai, Biên Hòa, Đồng Nai",
   "Longtitude": 10.967755,
   "Latitude": 106.853676
 },
 {
   "STT": 10,
   "Name": "Phòng khám Đa khoa Tâm An",
   "address": "E43 - E44 khu dân cư Võ Thị Sáu, đường D9, Thống Nhất, Biên Hòa, Đồng Na",
   "Longtitude": 10.9487122,
   "Latitude": 106.8299871
 },
 {
   "STT": 11,
   "Name": "Phòng khám đa khoa Hoàng Anh Đức",
   "address": "Khu phố 1, quốc lộ 1A, Lòng Đình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.947167,
   "Latitude": 106.8691099
 },
 {
   "STT": 12,
   "Name": "Phòng khám Đa khoa Quốc tế Sỹ Mỹ - Chi nhánh Hố Nai",
   "address": "116/4 quốc lộ 1A, khu phố 10, Tân Biên, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9696455,
   "Latitude": 106.8970468
 },
 {
   "STT": 13,
   "Name": "Phòng khám Đa khoa Ái Nghĩa - Chi nhánh 4",
   "address": "122 - 124 Đồng Khởi, Tân Phong, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9808972,
   "Latitude": 106.8475942
 },
 {
   "STT": 14,
   "Name": "Nhà thuốc - Phòng khám Đa khoa Tân Long",
   "address": "06 - 23 - 24/F6 khu phố 1, Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9007904,
   "Latitude": 106.8537814
 },
 {
   "STT": 15,
   "Name": "Phòng khám Đa khoa Quốc tế Sỹ Mỹ - Chi nhánh 3",
   "address": "55-56/1, Bình Đa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9355815,
   "Latitude": 106.8614105
 },
 {
   "STT": 16,
   "Name": "Phòng khám Đa khoa Công Minh",
   "address": "151/9 Bùi Trọng Nghĩa, khu phố 2, Trảng Dài, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9838413,
   "Latitude": 106.8624356
 },
 {
   "STT": 17,
   "Name": "Phòng khám Đa khoa Y Sài Gòn",
   "address": "221 Đồng Khởi, Tam Hiệp, Biên Hòa, Đồng Nai ",
   "Longtitude": 10.9576411,
   "Latitude": 106.8583892
 },
 {
   "STT": 18,
   "Name": "Phòng khám Đa khoa Sài Gòn - Gia Định",
   "address": "99/1 Phạm Văn Thuận, khu phố 3, Tam Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9527314,
   "Latitude": 106.8534028
 },
 {
   "STT": 19,
   "Name": "Phòng khám Đa khoa An Bình",
   "address": "4 Bùi Văn Hòa, An Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9321168,
   "Latitude": 106.87478
 },
 {
   "STT": 20,
   "Name": "Phòng khám Đa khoa - Bệnh xá Trường Cao đẳng nghề số 8",
   "address": "Bùi Văn Hòa, Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.907725,
   "Latitude": 106.8899311
 },
 {
   "STT": 21,
   "Name": "Phòng khám Đa khoa Pouchen",
   "address": "2429 Nguyễn Ái Quốc, Tân Tiến, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9577411,
   "Latitude": 106.8302214
 },
 {
   "STT": 22,
   "Name": "Phòng khám Đa khoa Duy Khang",
   "address": "A1-9 A1-10 khu chợ Mới Long Thành, Long Thành, Biên Hòa, Đồng Nai",
   "Longtitude": 10.7770943,
   "Latitude": 106.956174
 },
 {
   "STT": 23,
   "Name": "Phòng khám Đa khoa Bách Thư",
   "address": "126 Phan Đình Phùng, Thanh Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9522494,
   "Latitude": 106.8181946
 },
 {
   "STT": 24,
   "Name": "Phòng khám Đa khoa Quốc tế Sỹ Mỹ - Chi nhánh Đức Trí",
   "address": "9A Đồng Khởi, khu phố 3, Tân Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9550777,
   "Latitude": 106.8652559
 },
 {
   "STT": 25,
   "Name": "Phòng khám Đa khoa quốc tế Sỹ Mỹ - Chi nhánh 3",
   "address": "55/1 Phạm Văn Thuận, Bình Đa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9401911,
   "Latitude": 106.8677205
 },
 {
   "STT": 26,
   "Name": "Phòng khám Đa khoa Hoàng Anh Đức",
   "address": "Trung đoàn 22, quân đoàn 4, Long Bình, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9587097,
   "Latitude": 106.8792292
 },
 {
   "STT": 27,
   "Name": "Phòng khám Đa khoa Long Bình Tân",
   "address": "2D2 khu phố Bình Dương, Quốc lộ 51, Long Bình Tân, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9023675,
   "Latitude": 106.8497614
 },
 {
   "STT": 28,
   "Name": "Phòng khám Đa khoa Hà Phương",
   "address": "Tân Hiệp, Tân Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9623154,
   "Latitude": 106.8679899
 },
 {
   "STT": 29,
   "Name": "Phòng khám Đa khoa Sùng Đức",
   "address": "26/116A - 26/116B khu phố 3, Tam Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9540401,
   "Latitude": 106.8491516
 },
 {
   "STT": 30,
   "Name": "Phòng khám Đa khoa Thái Sơn",
   "address": "99 Phạm Văn Thuận, Tân Tiến, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9582024,
   "Latitude": 106.8332498
 },
 {
   "STT": 31,
   "Name": "Phòng khám Đa khoa Hiếu Nghĩa",
   "address": "124 - 122 Đồng Khởi, khu phố 4, Tân Phong, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9809642,
   "Latitude": 106.8475829
 },
 {
   "STT": 32,
   "Name": "Phòng khám Đa khoa Bửu Hòa",
   "address": "Bùi Hữu Nghĩa, Bửu Hòa, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9308798,
   "Latitude": 106.8201106
 },
 {
   "STT": 33,
   "Name": "Phòng khám Đa khoa Quân y 22",
   "address": "Quốc lộ 1A, Tân Hiệp, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9554373,
   "Latitude": 106.8751849
 },
 {
   "STT": 34,
   "Name": "Phòng khám Đa khoa Tam Hiệp",
   "address": "B15/4 Phạm Văn Thuận, Tân Tiến, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9413182,
   "Latitude": 106.8623827
 },
 {
   "STT": 35,
   "Name": "Phòng khám Đa khoa Quốc tế Sỹ Mỹ - Cơ sở 2",
   "address": "159 Cách Mạng Tháng Tám, khu phố 3, Quyết Thắng, Biên Hòa, Đồng Nai",
   "Longtitude": 10.9434983,
   "Latitude": 106.8209456
 }
];